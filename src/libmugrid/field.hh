/**
 * @file   field.hh
 *
 * @author Till Junge <till.junge@altermail.ch>
 *
 * @date   10 Aug 2019
 *
 * @brief  Base class for fields
 *
 * Copyright © 2019 Till Junge
 *
 * µGrid is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3, or (at
 * your option) any later version.
 *
 * µGrid is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with µGrid; see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * Additional permission under GNU GPL version 3 section 7
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with proprietary FFT implementations or numerical libraries, containing parts
 * covered by the terms of those libraries' licenses, the licensors of this
 * Program grant you additional permission to convey the resulting work.
 *
 */

#ifndef SRC_LIBMUGRID_FIELD_HH_
#define SRC_LIBMUGRID_FIELD_HH_

#include "exception.hh"
#include "grid_common.hh"

#include <string>
#include <typeinfo>

namespace muGrid {

  /**
   * base class for field-related exceptions
   */
  class FieldError : public RuntimeError {
   public:
    //! constructor
    explicit FieldError(const std::string & what) : RuntimeError(what) {}
    //! constructor
    explicit FieldError(const char * what) : RuntimeError(what) {}
  };

  //! forward-declaration
  class FieldCollection;

  //! forward-declaration
  class StateField;

  /**
   * Abstract base class for all fields. A field provides storage discretising a
   * mathematical (scalar, vectorial, tensorial) (real-valued,
   * integer-valued, complex-valued) field on a fixed number of quadrature
   * points per pixel/voxel of a regular grid.
   * Fields defined on the same domains are grouped within
   * `muGrid::FieldCollection`s.
   */
  class Field {
   protected:
    /**
     * `Field`s are supposed to only exist in the form of `std::unique_ptr`s
     * held by a FieldCollection. The `Field` constructor is protected to
     * ensure this.
     * @param unique_name unique field name (unique within a collection)
     * @param nb_dof_per_sub_pt number of components to store per quadrature
     * point
     * @param collection reference to the holding field collection.
     */
    Field(const std::string & unique_name, FieldCollection & collection,
          const Dim_t & nb_dof_per_sub_pt, const Dim_t & nb_sub_pts,
          const PixelSubDiv & subdivision, const Unit & unit);

   public:
    //! Default constructor
    Field() = delete;

    //! Copy constructor
    Field(const Field & other) = delete;

    //! Move constructor
    Field(Field && other) = default;

    //! Destructor
    virtual ~Field() = default;

    //! Copy assignment operator
    Field & operator=(const Field & other) = delete;

    //! Move assignment operator
    Field & operator=(Field && other) = delete;

    //! return the field's unique name
    const std::string & get_name() const;

    //! return a const reference to the field's collection
    FieldCollection & get_collection() const;

    //! return the number of components stored per quadrature point
    const Dim_t & get_nb_dof_per_sub_pt() const;

    //! return the number of sub points per pixel
    const Dim_t & get_nb_sub_pts() const;

    //! return the number of components stored per pixel
    Dim_t get_nb_dof_per_pixel() const;

    //! return the number of pixels
    Dim_t get_nb_pixels() const;

    /**
     * returns the number of entries held by any given field in this
     * collection. This corresponds to nb_pixels × nb_sub_pts, (I.e., a scalar
     * field field and a vector field sharing the the same collection have the
     * same number of entries, even though the vector field has more scalar
     * values.)
     */
    Dim_t get_nb_entries() const;

    /**
     * evaluate and return the overall shape of the field (for passing the
     * field to generic multidimensional array objects such as numpy.ndarray)
     */
    std::vector<Dim_t> get_shape(const PixelSubDiv & iter_type) const;

    /**
     * evaluate and return the overall shape of the pixels portion of the field
     * (for passing the field to generic multidimensional array objects such as
     * numpy.ndarray)
     */
    std::vector<Dim_t> get_pixels_shape() const;

    /**
     * evaluate and return the shape of the data contained in a single pixel or
     * sub-point (e.g. quadrature point) (for passing the field to generic
     * multidimensional array objects such as numpy.ndarray)
     */
    virtual std::vector<Dim_t>
    get_components_shape(const PixelSubDiv & iter_type) const;

    /**
     * evaluate and return the overall strides of the pixels portion of the field
     * (for passing the field to generic multidimensional array objects such as
     * numpy.ndarray)
     */
    std::vector<Dim_t> get_pixels_strides() const;

    /**
     * evaluate and return the number of components in an iterate when iterating
     * over this field
     */
    Dim_t get_stride(const PixelSubDiv & iter_type) const;

    /**
     * evaluate and return the number of rows of a default iterate over this
     * field. Warning, this function does no sanity checks at all. It is assumed
     * that the user called `get_stride` before, that all checks have been
     * performed there, and that rechecking would be a waste of time)
     */
    Dim_t get_default_nb_rows(const PixelSubDiv & iter_type) const;

    /**
     * evaluate and return the number of cols of a default iterate over this
     * field. Warning, this function does no sanity checks at all. It is assumed
     * that the user called `get_stride` before, that all checks have been
     * performed there, and that rechecking would be a waste of time)
     */
    Dim_t get_default_nb_cols(const PixelSubDiv & iter_type) const;

    /**
     * return the type information of the stored scalar (for compatibility
     * checking)
     */
    virtual const std::type_info & get_stored_typeid() const = 0;

    //! number of entries in the field (= nb_pixel × nb_sub_pts)
    size_t size() const;

    //! size of the internal buffer including the pad region (in scalars)
    virtual size_t buffer_size() const = 0;

    /**
     * add a pad region to the end of the field buffer; required for using this
     * as e.g. an FFT workspace
     */
    virtual void set_pad_size(const size_t & pad_size_) = 0;

    //! pad region size
    const size_t & get_pad_size() const;

    /**
     * initialise field to zero (do more complicated initialisations through
     * fully typed maps)
     */
    virtual void set_zero() = 0;

    /**
     * checks whether this field is registered in a global FieldCollection
     */
    bool is_global() const;


    //! check wether the number of pixel sub-divisions has been set
    bool has_nb_sub_pts() const;

    //! returns a const ref to the field's pixel sub-division type
    const PixelSubDiv & get_sub_division() const;

   protected:
    //! gives field collections the ability to resize() fields
    friend FieldCollection;

    //! sets the number of sub points per pixel
    void set_nb_sub_pts(const Dim_t & nb_quad_pts_per_pixel);


    /**
     * maintains a tally of the current size, as it cannot be reliably
     * determined from either `values` or `alt_values` alone.
     */
    size_t current_size{};

    //! resizes the field to the given size
    virtual void resize() = 0;

    const std::string name;  //!< the field's unique name

    //! reference to the collection this field belongs to
    FieldCollection & collection;

    /**
     * number of components stored per sub-point (e.g., 3 for a
     * three-dimensional vector, or 9 for a three-dimensional second-rank
     * tensor)
     */
    const Dim_t nb_dof_per_sub_pt;

    //! size of padding region at end of buffer
    size_t pad_size{};

    /**
     * number of pixel subdivisions. Will depend on sub_division
     */
    Dim_t nb_sub_pts;

    /**
     * Pixel subdivision kind (determines how many datapoints to store per
     * pixel)
     */
    PixelSubDiv sub_division;

    //! Physical unit of the values stored in this field
    Unit unit;
  };

}  // namespace muGrid

#endif  // SRC_LIBMUGRID_FIELD_HH_
